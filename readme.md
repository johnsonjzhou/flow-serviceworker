# flow-serviceworker

A Service Worker that uses the Cache API so that apps can continue to run 
with poor network connectivity.  

Future work:  
- Integrate `flow-idbworker` and handle offline `POST` requests.  

---
## Version
1.0.4  [Changelog](./doc/changelog.md)

---
## Install  

````bash
npm install https://bitbucket.org/johnsonjzhou/flow-serviceworker.git
````

---
## Use  

This package contains two elements:  
- [ServiceWorker](./doc/sw.md), which implements 
[CacheWorker](./doc/cache_worker.md), and  
- [ServiceWorkerHandler](./doc/sw_handler.md).  

***ServiceWorker***  
The actual Service Worker. This must be built on it's own with a separate 
entry point to the main application.  

***ServiceWorkerHandler***  
Can be imported into the main application and used as a Class.  

---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  