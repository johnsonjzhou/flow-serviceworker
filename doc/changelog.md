# 'flow-serviceworker' changelog

## 1.0.4  

Changes:  
- Increased fromNetwork timeout to 3000ms  
- respond408 will include an error message if TypeError is caught  
- Refactored fetch event listener to be more readable  
- GET requests where cache is explicitly avoided will use fromNetworkPersisting policy  
- Console will log status and url when fetch fails that results in the network state being set to offline, for debugging purposes  
- respond408 will write error to Console if error is caught  
- refactored no-cache GET request to networkOnly  
- cacheThenNetwork, failed network update after successful cache response need not set the network offline  
- Request.mode=navigate will always be cacheOnly  
- Failed fetch will correctly show the request url in the Console.log  
- Request url starting with 'chrome' will not be handled  
- Manifest.json example renamed as example.webmanifest to keep in line with standard, also includes .htaccess hint for MIME type  

Fixes:  
- Network does not switch back online due to faulty logic in setNetworkOnline  

## 1.0.3  
Fix issue whereby if a `NetworkError` occurs during `fromNetworkPersisting`, 
this will throw an error due to the `event.respondWith` handler resolving 
to a rejected Promise. This is now handled by responding with a blank response 
with **ok = false** and **status = 408**.  

## 1.0.2  
Fix issue with `___WEBPACK_INJECT_MANIFEST___` whereby `InjectManifest` will 
inject the build manifest into the first instance, which was in the 
comment block.  

Also updated docs to show example of how to avoid files from being injected.  

## 1.0.1  
Fix import error from `flow-utils`  

## 1.0.0  
Initial commit.  
