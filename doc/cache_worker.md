# CacheWorker  

Wrapper functions to handle `Cache API` with Service Worker. This implements  
[`Console`](https://bitbucket.org/johnsonjzhou/flow-utils/) to handle errors.  

> **Note**  
> This is intended to be used by the Service Worker rather than standalone.  

---
## Use  
````javascript
import CacheWorker from './cache_worker';

const Cache = new CacheWorker(cache_name);
// Cache API uses 'caches' as the global variable  
````

---
## Parameter(s)

***cache_name*** `string`  
The name of the Cache. This can be anything, but using a version is useful.  

--- 
## Methods of the constructor  
- [CacheWorker.open](#cacheworker.open)  
- [CacheWorker.precache](#cacheworker.precache)  
- [CacheWorker.put](#cacheworker.put)  
- [CacheWorker.get](#cacheworker.get)  
- [CacheWorker.upgrade](#cacheworker.upgrade)  

---
## CacheWorker.open  
Open a cache within a Service Worker.  

### Parameter(s)  
None.  

### Return value(s)  
`Promise` that resolves to the `Cache` object.  

--- 
## CacheWorker.precache
Precache a collection of files based on the manifest array.  

### Parameter(s)  

***manifest*** `array`  
Array of URLs to precache.  

### Return value(s)  
`Promise` resolving to `null`.  

### Exceptions  
All exceptions are caught and handled by `Console.log`.  

--- 
## CacheWorker.put  
Adds (put) a response for a specific request to the cache.  

### Parameter(s)  

***request*** `object`  
A `Request` object containing the request `url`.  

***response*** `object`  
A `Response` object that will be put into the cache.  

### Return value(s)  
`Promise` resolving to `null`.  

---
## CacheWorker.get  
Retrieves (get) a specific request from the cache if available.  

### Parameter(s)  

***request*** `object`  
A `Request` object containing the request `url`.  

### Return value(s)  
`Promise` resolving to the cached `Response`. If the request is not cached, 
then a `Response` with status code of `404` and data of `null` will be returned.  

--- 
## CacheWorker.upgrade  
'Upgrades' the cache system by deleting all caches not corresponding to 
the current cache name.  

> Call this with `event.waitUntil` method of the `activate` event 
> of the service worker.  

### Parameter(s)  
None.  

### Return value(s)  
`Promise` that resolves to `null` once all existing caches have been deleted.  

---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  