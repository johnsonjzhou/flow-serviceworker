# ServiceWorker  

The service worker to handle Fetch events. Depending on the file type and 
network status, the behaviour may be **Cache then Network** or 
**Network then Cache**. This implements 
[`Console`](https://bitbucket.org/johnsonjzhou/flow-utils/) to handle errors.  

Future work:  
- Implement `flow-idbworker` to handle offline `post` and `update` requests.  

---  
## 408: Response Timeout  

> **Network Errors**  
> All network errors intercepted by the service worker will be transformed 
> to a Response with status of *408: Request Timeout*.  

In case where **fetch** events result in a network error, the service worker 
will respond with a blank `Response` object with the following parameters.  

Parameter | Value  
-- | --  
`ok` | `false`  
`status` | `408`  

---
## Cache then Network  

The request is served from the Cache immediately then updated from the 
network.  

This method is used for:  
- `get` requests when network is **offline**, or  
- `get` request on a defined [Static Asset](#staticasset).  

**Cache available**  
1. Respond to client with cached resource.  
2. Fetch update from network.  
3. If the network responds with status `304: Not Modified`, skip following.  
4. Save updated resource in cache.  
5. Broadcast to all clients that a resource has changed. 
See [Broadcasts](#broadcasts).  
6. Clients have the option to obtain the updated resource by fetching 
using the custom `refresh` method. See [Refresh](#refresh).  

**Cache not available**  
1. Fetch from network in a persisting manner (no set timeout). This works 
like a standard Fetch event. The `Response` promise will not resolve until 
network responds or the browser terminates the request.  

---
## Network then Cache

Attempt to access the network for the request, with a timeout of **2 seconds**. 
If network is inaccessible or timeout is reached, then attempt to load the 
resource from cache.  

This method is used for:  
- `get` requests when network is **online**, or  
- all other request types.  

**Network responds within timeout**  
1. Respond to client with network resource.  
2. Save the network resource to cache.  

**Network timeout exceeded, cache available**  
1. Respond with cached resource.  

**Network timeout exceeded, cache not available**  
1. Fetch from network in a persisting manner (no set timeout). This works 
like a standard Fetch event. The `Response` promise will not resolve until 
network responds or the browser terminates the request.  

---
## Network status

Network status be either **online** or **offline**.  

The network status is updated everytime the network is accessed for 
a resource. When the network status is **changed**, all clients will 
receive a message indicating the change. See [Broadcasts](#broadcasts).  

As a safety measure, network status broadcasts has a minimum gap interval 
of **5 seconds**.  

---
## Broadcasts  

### A resource has changed  
````javascript
client.postMessage({ url });
````

***url*** `string`  
The `url` of the original request.  

### Network status has changed  
````javascript
client.postMessage({ online, updated });
````

***online*** `bool`  
Whether we are online (`true`) or offline (`false`).

***updated*** `string`  
Timestamp as returned by `Date.now()`.

---
## Refresh  

`Refresh` is a custom request method handled by the Service Worker. This is 
intended to be used by the client upon receiving a **resource has changed** 
broadcast and is responded exclusively from the cache.  

### Reason for this method  
Some response types, eg. images, could not be serialised when using
`client.postMessage()`, and will throw a `DOMException: Response object 
could not be cloned`.  

This prevents clients being updated via broadcast in the **Cache then Network** 
mode.  

We are getting around this by notifying clients that an update exists
for a given `url`, then the clients can submit a fetch event using the
`refresh` method, which then the Service Worker will only serve up the updated
copy from the cache.  

This of course requires that the cache be updated from the network 
before clients are notified.  

---
## First Load Assets for pre-cache

First load assets is an array of URL paths that the Service Worker will 
precache during it's `install` lifecycle. It is a merger of two arrays 
defined during Webpack build.  

Webpack Constant | Description 
--- | --- 
`___WEBPACK_SW_ASSETS___` | Manually defined `array` of assets. 
`___WEBPACK_INJECT_MANIFEST___` | An `array` of assets built by the Webpack process. 

---
## Static Assets  

Static assets are resources that are rarely updated. For maximum speed, 
these resources will always be served using the `Cache then Network` method.  

Since the Service Worker will never access the network to fetch updates, it is 
important to ensure these resources are part of the build manifest for the 
Service Worker and then updates are pre-cached as part of the `install` 
lifecycle of the Service Worker.

Webpack Constant | Description 
--- | --- 
`___WEBPACK_SW_STATIC_FILE_TYPES___` | Manually defined `array` of assets. 

---
## Use Example 

Install dependencies.  
````bash
npm install webpack
npm install workbox-webpack-plugin
````

**webpack.js**  
````javascript
// using InjectManifest to build the sw.js as well as inject a list of assets
// @see https://developers.google.com/web/tools/workbox/modules/workbox-webpack-plugin
const { InjectManifest } = require('workbox-webpack-plugin');

// service worker version
const version = '1.0.0'; 

// manually defined list of assets to pre-cache
const firstLoadAssets = [
  '/', 
  'manifest.json'
];

// url paths containing these strings will be handled by cache-then-network policy
const staticFileTypes = [
  'eot', 'ttf', 'otf', 'woff', 'woff2', 'svg', 'png', 'jpg', 'jpeg', 
  'manifest.json',
  '/assets/fonts',
  '/api/content',
  '/pwa'
];

// do not inject these files with these names
const avoidInjection = [
  '.htaccess', 
  '403.php', '404.php', '500.php'
];

module.exports = {
  ...
  plugins: [
    new InjectManifest({
      swSrc: './node_modules/flow-serviceworker/src/sw.js',
      swDest: './dist/sw.js',
      injectionPoint: '___WEBPACK_INJECT_MANIFEST___',
      maximumFileSizeToCacheInBytes: 10000000,
      manifestTransforms: [
        async (manifestEntries) => {
          const manifest = manifestEntries
          .map(entry => {
            // strip path from url
            (entry.url.startsWith('assets/')) &&
            (entry.url = entry.url.substring(7));

            // avoid injecting these files 
            if (avoidInjection.includes(entry.url)) return;

            // add / to url if doesn't exist 
            if (!entry.url.startsWith('/')) 
            entry.url = '/' + entry.url;

            return entry.url;
          })
          .filter(url => url);
          return {manifest, warnings: []};
        },
      ]
    }),
    new webpack.DefinePlugin({
      ___WEBPACK_SW_VERSION___: JSON.stringify(version),
      ___WEBPACK_SW_ASSETS___: JSON.stringify(firstLoadAssets),
      ___WEBPACK_SW_STATIC_FILE_TYPES___: JSON.stringify(staticFileTypes),
    })
  ]
}
````

---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  