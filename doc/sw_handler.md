# ServiceWorkerHandler  

Handles service worker lifecycles in the main (app) thread. This implements  
[`Console`](https://bitbucket.org/johnsonjzhou/flow-utils/) to handle errors.  

Future work:  
- Consider using `Transferable` interface of `postMessage`  

---
## Use  
````javascript
import ServiceWorkerHandler from 'flow-serviceworker';

const sw = new ServiceWorkerHandler();
````

---  
## App updates  

When an active Service Worker registration is being handled within this class, 
updates to the Service Worker script will be checked according to the following 
schedule based on `process.env.NODE_ENV`.  

Mode | Update check timer  
-- | --  
Development | 10 seconds  
Production | 60 minutes  

---
## Methods of the ServiceWorkerHandler constructor

- [ServiceWorkerHandler.getRegistration](#serviceworkerhandler.getregistration)  
- [ServiceWorkerHandler.isRegistered](#serviceworkerhandler.isregistered)  
- [ServiceWorkerHandler.register](#serviceworkerhandler.register)  
- [ServiceWorkerHandler.setNewVersionallback](#serviceworkerhandler.setnewversioncallback)  
- [ServiceWorkerHandler.setMessageCallback](#serviceworkerhandler.setmessagecallback)  
- [ServiceWorkerHandler.skipWaiting](#serviceworkerhandler.skipwaiting)  
- [ServiceWorkerHandler.postMessage](#serviceworkerhandler.postmessage)  

---
## ServiceWorkerHandler.getRegistration

Checks the registration status of a Service Worker for a given scope, 
by getting the registration from `navigator.serviceWorker`. 
If exists, set `this.registration` as the retrieved registration. 

This is useful for non-registering classes or components to access 
the service worker messaging like `setMessageCallback` and `postMessage`.  

### Parameter(s)  

***scope*** `string`  
The scope of the Service Worker. Default is `'/'`.  

### Return value(s)
`Promise` that resolves to `true` or `false`.  

---
## ServiceWorkerHandler.isRegistered

checks whether a Service Worker is registered in this class.  

### Parameter(s)  
None.  

### Return value(s)  
`boolean`.  

---
## ServieWorkerHandler.register

Creates or updates the Service Worker registration using 
`navigator.serviceWorker.register`.  

### Parameter(s)  

These parameters are identical to and will be directly passed to 
`navigator.serviceWorker.register`.  

***scriptURL*** `string`  
The URL of the Service Worker script.  

***options*** `object`  
An object containing registration options.  

### Return value(s)  
Void.  

### Exceptions  
Caught and handled by `Console.notify`.  

---
## ServiceWorkerHandler.setNewVersionCallback

Sets the callback to use when a new version is found.  

### Parameter(s)  

***callback*** `function`  
The callback function.  

### Return value(s)  
Void.  

---
## ServiceWorkerHandler.setMessageCallback

Sets the callback to use when a message is received from the Service Worker, 
then add an event listener on the `message` event.  

The callback function will take one argument being the `data` object derived 
from the `MessageEvent`.  

Scenario | `data` properties 
-- | --
Resource has changed that was subject to **cache-then-network** policy. | ***url*** `string`  
Network status (online/offline) has changed. | ***online*** `bool`, ***updated*** `string`  


### Parameter(s)  

***callback*** `function`  
The callback function.  

### Return value(s)  
Void.  

---
## ServiceWorkerHandler.skipWaiting

Activates an updated service worker immediately (skipWaiting), 
by posting a message to it with data of **skipWaiting** in the **waiting** 
state.

### Parameter(s)  
None.

### Return value(s)  
Void.  

---
## ServiceWorkerHandler.postMessage

Posts a message to the Service Worker.  

### Parameter(s)  

***message*** `object`  
Contains two parameters.  
- `data` The body of the message.  
- `state` The state of the Service Worker to target. Typeof **active**, 
**installing**, **waiting**. Defaults to **active**.  

### Return value(s)  
Void.  

___
## Examples  

### Registering and interacting with the Service Worker (main context)  
````javascript
// main app context 
import ServiceWorkerHandler from 'flow-serviceworker';

const sw = new ServiceWorkerHandler();

sw.setNewVersionCallback(() => {
  // do this when a new version is detected
});

sw.setMessageCallback((data) => {
  // do this when a message is received from the Service Worker
});

sw.register({
  scriptURL: '/sw.js', 
  options: { scope: '/' }
});
````

### Communicating with the Service Worker (sub context)  
````javascript
// a sub process that uses a Static Asset that uses the cache-then-network 
// policy and need to be updated when the resource has changed 
import ServiceWorkerHandler from 'flow-serviceworker';

const sw = new ServiceWorkerHandler();

// the source for a given resource that needs to be fetched
const source = '/docs/example.md';

sw.setMessageCallback(({ url }) => {
  // if the url matches our source, update the resource by fetching again
  // using the custom 'refresh' method 
  url.includes(source) && 
  fetch(source, { method: 'refresh' })
  .then((response) => {
    // update the resource
  });
});

````

---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  