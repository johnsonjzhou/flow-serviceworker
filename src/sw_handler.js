/**
 * Handles service worker lifecycles in the App
 * 
 * @export  ServiceWorkerHandler  
 * 
 * @method  getRegistration
 * @method  isRegistered
 * @method  register
 * @method  setNewVersionCallback
 * @method  setMessageCallback
 * @method  skipWaiting
 * @method  postMessage
 */
import { Console } from 'flow-utils';

class ServiceWorkerHandler {

  constructor() {

    // Console
    this.Console = new Console('SW HANDLER', 'slategray');

    // getters
    this.getRegistration = this.getRegistration.bind(this);
    this.isRegistered = this.isRegistered.bind(this);

    // setters
    this.setNewVersionCallback = this.setNewVersionCallback.bind(this);
    this.setMessageCallback = this.setMessageCallback.bind(this);
    this.register = this.register.bind(this);

    // handlers
    this.handleInstalling = this.handleInstalling.bind(this);
    this.handleWaiting = this.handleWaiting.bind(this);
    this.handleActive = this.handleActive.bind(this);
    this.handleUpdateFound = this.handleUpdateFound.bind(this);
    this.handleControllerStateChange = 
    this.handleControllerStateChange.bind(this);
    this.handleClientUpdate = this.handleClientUpdate.bind(this);
    this.skipWaiting = this.skipWaiting.bind(this);
    this.postMessage = this.postMessage.bind(this);
    this.receiveMessage = this.receiveMessage.bind(this);
    this.checkForUpdate = this.checkForUpdate.bind(this);

    // service worker particulars
    this.scriptURL = null;            // assigned in register
    this.scope = null;                // assigned in register
    this.registration = null;         // assigned in register

    // refresh state when controllerchange (eg. activation of new version)
    this.refreshing = false;

    // callbacks
    this.newVersionCallback = null;
    this.messageCallback = null;

    // whether we are in production mode
    this.productionMode = 
    (process.env.NODE_ENV === 'production') ? true : false;
  }

  /**
   * Checks the registration status of a service worker for a given scope,
   * by getting the registration from navigator.serviceWorker,
   * if exists, set this.registration as the retrieved registration.
   * This is useful for non-registering classes or components to access
   * the service worker messaging like setMessageCallback and postMessage
   * @param  {string}  scope - default '/'
   * 
   * @return  {Promise} - resolves to true or false
   */
  getRegistration(scope = '/') {
    this.registration === null && 
    navigator.serviceWorker && 
    navigator.serviceWorker.getRegistration(scope)
    .then(registration => {
      // no service worker registered
      if (registration === 'undefined') return false;

      // registered service worker found
      this.Console.log(`Scope ${scope}`, 'Registration found');
      this.registration = registration;
      return true;
    })
  }

  /**
   * Checks whether a service worker is registered in this class
   * @return  {bool}
   */
  isRegistered() {
    return (
      this.registration && 
      typeof this.registration === 'object' && 
      this.registration.constructor.name === 'ServiceWorkerRegistration'
    );
  }

  /**
   * Creates or updates the service worker
   * @param  {object}  {scriptURL, options}
   */
  register({
    scriptURL = '/assets/sw.js', 
    options = {scope: '/'}
  }) {
    this.scriptURL = scriptURL;
    this.scope = options.scope;

    location.protocol === 'https:' &&
    navigator.serviceWorker &&
    navigator.serviceWorker
    .register(scriptURL, options)
    .then((registration) => {
      this.registration = registration;
      const {
        installing,
        waiting,
        active
      } = registration;
      // @see https://devdocs.io/dom/serviceworkerregistration
      installing && this.handleInstalling();
      waiting && this.handleWaiting();
      active && this.handleActive();
    })
    .catch(error => {
      // registration failed
      this.Console.notify(error, 'Register');
    });
  }

  /**
   * Sets the callback to use when a new version is found
   * @param  {function}  callback
   */
  setNewVersionCallback(callback) {
    // this.Console.log(callback, 'Set new version callback');
    this.newVersionCallback = callback;
  }

  /**
   * Sets the callback to use when a message is received from the
   * service worker, then add an event listener on the message event
   */
  setMessageCallback(callback) {
    this.messageCallback = callback;
    navigator.serviceWorker && 
    // (navigator.serviceWorker.onmessage = this.receiveMessage);
    navigator.serviceWorker.addEventListener('message', this.receiveMessage);
  }

  /**
   * Removes the message event listener that was previously set
   */
  unsetMessageCallback() {
    navigator.serviceWorker && 
    navigator.serviceWorker.removeEventListener('message', this.receiveMessage);
  }

  /**
   * Do these things when the registration is in 'installing' state
   * this may be mirrored in the 'install' event in the service worker itself
   */
  handleInstalling() {
    this.Console.log(this.scriptURL, 'Installing');
    this.handleUpdateFound();
  }

  /**
   * Do these things when the registration is in 'waiting' state
   */
  handleWaiting() {
    this.Console.log(this.scriptURL, 'Waiting to activate');
    this.newVersionCallback && 
    typeof this.newVersionCallback === 'function' &&
    this.newVersionCallback();
  }

  /**
   * Do these things when the registration is in 'active' state
   * this may be mirrored in the 'activate' event in the service worker itself
   * - check for updates at set intervals
   * - listen and handle when 'updatefound'
   */
  handleActive() {
    this.Console.log(this.scriptURL, 'Active');

    // set a timer to update the service worker 
    // 60 mins in production or 10 secs in development
    const duration = this.productionMode ? 3600000 : 10000;
    const interval = window.setInterval(this.checkForUpdate, duration);

    // set a clean up routine for when the client gets reloaded
    window.addEventListener('beforeunload', event => {
      window.clearInterval(interval);
    });

    // listen for updates on the registration
    this.registration && 
    (this.registration.onupdatefound = this.handleUpdateFound);
  }

  /**
   * Sends an update instruction for the service worker registration
   */
  checkForUpdate() {
    // this.Console.log(this.scriptURL, 'Checking for update');
    try {
      this.registration &&
      this.registration.update && 
      this.registration.update();
    } catch (error) {
      // ignore bad network requests when updating the service worker
      if (
        error instanceof TypeError || 
        error.constructor.name === 'AbortError'
      ) return;
      this.Console.handleError(error);
    }
  }

  /**
   * Handler for the 'updatefound' event on the registration,
   * if a callback is set, then call the callback with no parameters
   * 
   * @note  this.newVersionCallback can cause a Violation if the callback
   * causes a delay in completion
   */
  handleUpdateFound() {
    this.Console.log('Update found');

    // add a statechange listener for registration.installing
    this.registration && 
    (this.registration.installing.onstatechange = 
    this.handleControllerStateChange);
  }

  /**
   * Handles the onstatechange event of the service worker
   * @param  {event} - 'statechange' event
   */
  handleControllerStateChange(event) {
    const state = event.target.state;

    // listen for controllerchange event,
    // signifying that a new service worker just came into service
    // @note  addEventListener('controllerchange') doesn't work
    (state === 'installed') &&
    navigator.serviceWorker && 
    (navigator.serviceWorker.oncontrollerchange = this.handleClientUpdate);

    // call the newVersionCallback if set
    (state === 'installed') && 
    this.newVersionCallback && 
    typeof this.newVersionCallback === 'function' &&
    this.newVersionCallback();
  }

  /**
   * Updates the client by causing the window to reload
   * uses this.refreshing as a safeguard against potential endless loop
   */
  handleClientUpdate() {
    if (this.refreshing) return;
    this.Console.log('Reloading client', 'Client update');
    this.refreshing = true;
    window.location.reload();
  }

  /**
   * Activates an updated service worker immediately (skipWaiting),
   * by posting a message to it with data of 'skipWaiting',
   * match this to service worker's 'message' event
   */
  skipWaiting() {
    this.registration.waiting && 
    this.postMessage({state: 'waiting', data: 'skipWaiting'});
  }

  /**
   * Post a message to the service worker
   * @param  {object}  {data, state}
   * @param  {mixed}  data
   * @param  {string}  state - type of active | installing | waiting
   */
  postMessage({data, state= 'active'}) {
    data && 
    this.registration && 
    this.registration[state] && 
    this.registration[state].postMessage(data);
  }

  /**
   * Receive a message from the service worker,
   * apply the message data to this.messageCallback
   * @param  {MessageEvent}  - 'message' event
   */
  receiveMessage(event) {
    const { data } = event;
    this.messageCallback && 
    this.messageCallback(data);
  }

}

export {
  ServiceWorkerHandler as default
};