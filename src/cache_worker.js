/**
 * Wrapper functions to handle Cache API with Service Worker
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @export  CacheWorker
 * 
 * @method  open
 * @method  precache
 * @method  put  
 * @method  get  
 * @method  upgrade
 */
import { Console as ConsoleWorker } from 'flow-utils';

class CacheWorker {

  constructor(cache_name) {
    this.cache_name = cache_name;

    // Console
    this.Console = new ConsoleWorker('SW.JS CACHE', 'slategray');

    // handlers
    this.open = this.open.bind(this);
    this.precache = this.precache.bind(this);
    this.respondNotCached = this.respondNotCached.bind(this);
    this.put = this.put.bind(this);
    this.get = this.get.bind(this);
    this.upgrade = this.upgrade.bind(this);
  }

  /**
   * Open a cache within a Service Worker
   * @return  {Promise}  resolving to Cache
   */
  open() {
    return caches.open(this.cache_name)
    .then(cache => {
      //this.Console.log(`Opened cache ${this.cache_name}`, 'Open');
      return cache;
    });
  }

  /**
   * Precache a collection of files based on the manifest array
   * @param  {array}  manifest
   * @return  {Promise}  resolving to null
   */
  precache(manifest) {
    // skip if manifest is not an array
    if (!Array.isArray(manifest)) return;

    // open cache,
    // then use addAll() to add manifest files to cache
    return this.open()
    .then(cache => {
      return cache.addAll(manifest)
      .then(() => {
        this.Console.log(`Precached ${manifest.length} assets`, 'Precache');
      });
    })
    .catch(error => this.Console.log(error));
  }

  /**
   * Returns an empty response with status code of 404 Not Found
   * @return  {Response}
   */
  respondNotCached() {
    return new Response(null, {status: 404});
  }

  /**
   * Adds (put) a response for a specific request to the cache
   * @param  {Request}  request
   * @param  {Response}  response
   * @return  {Promise}  resolving to null
   */
  put(request, response) {
    const url = request.url;

    // avoid chrome-extension as it is not supported
    if (url.startsWith('chrome-extension')) return;

    // open cache,
    // then put() the response into cache
    // clone the response so it is not consumed
    return this.open()
    .then(cache => {
      return cache.put(request.clone(), response.clone());
      //.then(() => this.Console.log(url, 'Saved'));
    })
    .catch(error => {
      // should not throw anything from caches.open
      // per docs, if cache with the requested name does not exist,
      // a new cache will be created with that name
    });
  }

  /**
   * Retrieves (get) a specific request from the cache if available
   * @param  {Request}  request
   * @return  {Promise}  - resolves to a Response
   * 
   * @note  response.status will be 404 if not in cache
   */
  get(request) {
    return this.open()
    .then(cache => {
      return cache.match(request.clone())
      .then(cachedResponse => {
        // this is an intermediate step to ensure that a Response
        // object is always served. cache.match will resolve to 
        // undefined if not found

        // if no matching cachedResponse,
        // return a new Response with status of 404
        if (cachedResponse === undefined) 
        return this.respondNotCached();

        // if matching, serve the matching response
        this.Console.log(request.url, 'Retrieved');
        return cachedResponse;
      })
      .catch(error => this.Console.log(error));
    })
    .catch(error => {
      // should not throw anything from caches.open
      // per docs, if cache with the requested name does not exist,
      // a new cache will be created with that name
    });
  }

  /**
   * 'Upgrades' the cache system by deleting all caches not 
   * corresponding to the current cache name.
   * Call this with event.waitUntil method of the activate event 
   * of the service worker.
   */
  upgrade() {
    return caches.keys()
    .then(keyList => Promise.all(
      keyList.map(key => {
        if (key !== this.cache_name) {
          this.Console.log(`Deleted cache ${key}`, 'Upgrade')
          return caches.delete(key);
        }
      })
    ))
  }
}

export {
  CacheWorker as default
};