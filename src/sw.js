/**
 * Service Worker
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @constant  {string}  ___WEBPACK_SW_VERSION___
 * @constant  {array}   ___WEBPACK_SW_ASSETS___
 * @constant  {array}   ___WEBPACK_SW_STATIC_FILE_TYPES___
 * @constant  {array}   ___WEBPACK_INJECT_MANIFEST__
 * 
 * GET when Request.mode=navigate: cacheOnly
 * GET when Request.cache explicitly avoided: networkOnly
 * GET handling static file types (as defined): cacheThenNetwork
 * GET when Network.online = true: networkThenCache
 * GET when Network.online = false: cacheThenNetwork
 * 
 * cacheThenNetwork behaviour
 * 1. fromCache -> 2a|2b
 * 2a. cacheRetrieved -> respondWith(cache) && update fromNetwork
 * 2b. notCached -> respondWith(fromNetworkPersisting)
 * 3. content changed, broadcast to clients
 * 4. clients have the option of calling fetch using REFRESH method
 *    to get the updated content
 * 
 * networkThenCache behaviour
 * 1. Check network (2s timeout) -> 2a|2b
 * 2a. Network response within 2s -> respondWith(network), Network.online=true
 * 2b. Network timeout -> fromCache -> 3a|3b, Network.online=false
 * 3a. cacheRetrieved -> respondWith(cache)
 * 3b. notCached -> respondWith(fromNetworkPersisting)
 * 
 * todo:  integrate flow-idbworker for POST requests
 * todo:  consider using Transferrable interface for postMessage
 */
import CacheWorker from './cache_worker';
import { Console as ConsoleWorker } from 'flow-utils';

// webpack injected constants by 
const WEBPACK_SW_VERSION = ___WEBPACK_SW_VERSION___;
const WEBPACK_SW_ASSETS = ___WEBPACK_SW_ASSETS___;
const WEBPACK_SW_STATIC_FILE_TYPES = ___WEBPACK_SW_STATIC_FILE_TYPES___;

// webpack injected manifest by workbox-webpack-plugin.InjectManifest
const WEBPACK_INJECT_MANIFEST = ___WEBPACK_INJECT_MANIFEST___;

const Version = WEBPACK_SW_VERSION || '0.0.0';
const Console = new ConsoleWorker('SW.JS', 'slategray');
const Cache = new CacheWorker(Version);

// are we online or offline?
// timestamp that we last updated
let Network = {
  online: true,
  updated: null
};

// array of static assets that should be precached 
const firstloadAssets = WEBPACK_SW_ASSETS || [
  '/',
  '/manifest.json'
];

// static asset file types, rarely updated
// these file types we will always use cache-then-network method
// for maximum speed
const staticFileTypes = WEBPACK_SW_STATIC_FILE_TYPES || [
  // 'eot', 'ttf', 'otf', 'woff', 'woff2', 'svg', 'png', 'jpg', 'jpeg', 
  'manifest.json',
];

/**
 * When the Service Worker is installed
 * - Save all precache items into Cache
 */
self.addEventListener("install", event => {
  Console.log(Version, 'Installed');
  // precache asset manifest combines firstloadAssets 
  // and WEBPACK_INJECT_MANIFEST
  const manifest = firstloadAssets.concat(WEBPACK_INJECT_MANIFEST || []);
  event.waitUntil(Cache.precache(manifest));
});

/**
 * When the Service Worker is activated
 * - Upgrade the Cache
 * - Take control of all running clients without waiting
 */
self.addEventListener("activate", event => {
  Console.log(Version, 'Activated');
  
  // 'upgrade' means remove cache belonging to an older version of sw
  event.waitUntil(Cache.upgrade());

  // immediately take control of the client upon activation
  // usually waits until reload
  event.waitUntil(clients.claim());
});

/**
 * Message event handler (when a message is received from the main thread)
 * - skipWaiting
 * 
 * @param  {MessageEvent}  event
 */
self.addEventListener('message', event => {
  const {
    data
  } = event;
  Console.log(data, 'Message');

  // if message is 'skipWaiting', then we will call skipWaiting
  // usually this is called by the handler to activate a new sw version
  // that is in waiting state
  if (data === 'skipWaiting') return self.skipWaiting();
});

/**
 * Fetch event handler
 * - GET
 * - REFRESH - this is a custom method
 * todo: POST
 */
self.addEventListener("fetch", event => {
  const {
    url, 
    method, 
    cache, 
    mode
  } = event.request; 

  // is the item being fetched of a static file type?
  const staticAsset = isStaticAsset({url, types: staticFileTypes});

  // avoid handling if its a chrome-extension 
  if (url.startsWith('chrome')) return; 

  switch (method) {
    case 'GET':

      // always respond from cache-only with navigate request modes 
      // this should correspond to page load 
      if (mode === 'navigate') return cacheOnly(event);

      // only fetch from network if cache usage is explicitly avoided 
      if (
        cache === 'no-store' || 
        cache === 'reload' || 
        cache === 'no-cache' 
      ) return networkOnly(event);

      // use cache-then-network
      // when it is a staticAsset or when network is offline, 
      // else use network-then-cache 
      if (staticAsset || Network.online === false) 
      return cacheThenNetwork(event);
      
      return networkThenCache(event);

    // REFRESH custom method,
    // this will refresh a GET call from the cache 
    // should only be called after a client has been notified that 
    // updated content exists
    // @see 'Dev Notes' at bottom
    case 'REFRESH':
      return cacheOnly(event);

    case 'POST':
      // todo: different method for handling POST calls
      // @see  https://serviceworke.rs/request-deferrer.html
      break;
  }
});

/**
 * Only respond from network 
 * @return {Promise} - resolves to a Response 
 */
const networkOnly = (event) => {
  event.respondWith(
    fromNetworkPersisting(event.request.clone())
  );
};

/**
 * Checks network, wait 3s timeout
 * if timeout, load cache
 * @return  {Promise} - resolves to a Response
 */
const networkThenCache = (event) => {
  // Console.log('Network then Cache', 'Fetch Handler');
  event.respondWith(
    fromNetwork(event.request.clone(), 3000)
    .then(response => {
      const {
        status
      } = response;

      // online response
      // if our state is offline, make it online
      // broadcast network state to clients
      if (status !== 408) {
        setNetworkOnline(true);
        return response;
      }

      // offline response
      // if our state is online, make it offline
      // broadcast network state to clients
      // then try the cache
      Console.log(`${status} ${event.request.url}`, 'Network-Then-Cache');
      setNetworkOnline(false);
      return fromCache(event.request.clone())
      .then(response => {
        const {
          status
        } = response;

        // in cache
        if (status !== 408) return response;

        // not in cache
        // at this stage, we have tried network (failed),
        // we have also tried cache (failed),
        // we will defer to default behaviour is to keep fetching
        if (status === 408) 
        return fromNetworkPersisting(event.request.clone());
      });
    })
  );
}

/** 
 * Handles a fetch event by responding immediately from the cache then
 * accesses the network for an update.
 * If an update is found, notify all clients that an update exists.
 * @param  {event}  event
 * @return  {Promise}  - resolves to a Response
 */
const cacheThenNetwork = (event) => {

  // we want to wait for cache retrieval 
  // before firing off a network request
  let cacheRetrieved;  // promise resolver
  let notCached;       // promise rejector
  const waitForCache = new Promise((resolve, reject) => {
    cacheRetrieved = resolve;
    notCached = reject;
  });

  Console.log(event.request.url, 'Cache Then Network');

  // immediately respond with cache
  event.respondWith(
    fromCache(event.request.clone())
    .then(response => {
      const {
        status 
      } = response;

      // in cache
      if (status !== 408) {
        // Flag that we have retrieved the cache
        // and pass on a copy of the response
        cacheRetrieved(response.clone());
        return response;
      }

      // not in cache
      // if it's not in the cache, reject the wawitForCache promise
      // so it doesn't keep waiting, then proceed to retrieve
      // the response from the network
      if (status === 408) {
        notCached();
        return fromNetworkPersisting(event.request.clone())
        .then(response => {
          const {
            status
          } = response;
    
          // online response
          // if our state is offline, make it online
          // broadcast network state to clients
          (status !== 408) && 
          setNetworkOnline(true);
    
          // offline response
          // if our state if online, make it offline
          // broadcast network state to clients
          Console.log(`${status} ${event.request.url}`, 'Cache-Then-Network, Not Cached');
          (status === 408) && 
          setNetworkOnline(false);

          return response;
        });
      }
    })
  )

  // the event will continue whilst we check the network for updates
  // we will wait for the cache first
  // waitForCache is a promise that will resolve when cacheRetrieved or
  // reject when notCached
  // we then send the ETag of the cache version to the server 
  // and the server will only update us if the file has changed 
  // to save on network bandwidth
  // network timeout 10s
  event.waitUntil(
    waitForCache
    // waitForCache resolves due to cacheRetrieved
    .then(cacheResponse => {
      // is there an ETag in the cacheResponse?
      let ETag = cacheResponse && 
      cacheResponse.headers && 
      cacheResponse.headers.get('ETag') || null;

      // create a clone of the event request headers
      // as the original is Immutable
      let networkHeaders = new Headers(event.request.headers);

      // if there is an ETag value, we will send this in the request 
      // within the 'If-None-Match' header
      // if matching, the server will return a 304 Not Modified response
      // which can save on unnecessary re-downloading of the same thing
      ETag !== null && 
      networkHeaders && 
      networkHeaders.set('If-None-Match', ETag); 

      // create a clone of the request to be used as networkRequest
      // with the added ETag values if necessary
      // the original request is immutable
      let networkRequest = new Request(event.request, {
        headers: networkHeaders
      });

      // query the network to update the request
      return fromNetwork(networkRequest, 10000)
      .then(response => {
        const {
          status, 
          ok,
          url
        } = response;
  
        // online response
        // if our state is offline, make it online
        // broadcast network state to clients
        if (status !== 408) {
          setNetworkOnline(true);

          // if we get an online ok response status 200-299
          // this means content has changed (not modified is 304)
          // we then broadcast this to clients
          ok && notifyResponseUpdate({url});

          return;
        }
      })
    })
    // if waitForCache is rejected due to notCached
    .catch(reason => null)
  )
}

/** 
 * Handles a fetch event by responding from only the cache,
 * no additional handling is done on the cache response,
 * as this assumes the Request will be available in the Cache.
 * @param  {event}  event
 * @return  {Promise}  - resolves to a Response
 */
const cacheOnly = (event) => {

  // transform a REFRESH method to a GET method
  const {
    method,
    url,
    ...originalRequest
  } = event.request;

  const cacheRequest = new Request(
    url,
    {
      method: ((method === 'REFRESH') && 'GET') || method,
      ...originalRequest
    }
  );
  
  // immediately respond with cache
  Console.log(`${method} ${url}`, 'Cache-Only');
  event.respondWith(
    fromCache(cacheRequest)
    .then(response => response)
  );
}

/**
 * Try the request from the online network,
 * apply a default timeout of 3000ms (timeout not in specs),
 * @param  {Request}  request - a http request
 * @param  {int}  timeout - timeout in milliseconds
 * @return  {Promise}  - resolves to Response, status 408 if timeout
 */
const fromNetwork = (request, timeout = 3000) => {
  return new Promise((resolve) => {
    const timeoutId = setTimeout(resolve, timeout, respond408());
    fetch(request.clone())
    .then(response => {
      const {
        ok, 
        status
      } = response;
      // Console.log(status, 'From Network');
      clearTimeout(timeoutId);

      // do not override the cache unless the response is ok (200-299)
      ok && Cache.put(request.clone(), response.clone());

      resolve(response.clone());
    })
    // When network is unavailable, fetch will return a TypeError
    // we will catch this and resolve the promise with a 408 Response
    // similar to if we reach the defined timeout
    .catch(error => resolve(respond408(error)));
  });
}

/**
 * Handling of cross origin requests
 * in this case, we will not cache or do anything to the request
 */
const fromNetworkPersisting = (request) => {
  return fetch(request.clone())
  .catch(error => respond408(error));
} 

/**
 * Try to retrieve the request from the CacheStorage,
 * return either the cached response or a 404 Response
 * @return  {Promise}  - resolves to Response, status 408 if not in cache
 */
const fromCache = (request) => {
  return new Promise((resolve) => {
    Cache.get(request.clone())
    .then(response => {
      // transform status of 404 to 408 for consistency
      // meaning not found in cache, but translates to offline for network
      const {
        status,
      } = response;

      (status !== 404) && resolve(response);

      (status === 404) && resolve(respond408());
    })
  });
}

/**
 * Returns an empty response with status code of 408 Request Timeout
 * @return  {Response}
 */
const respond408 = (error) => {
  error && Console.notify(`${error.name}\n${error.message}`, 'Respond-408');
  return new Response(
    error && `${error.name}\n${error.message}` || null, 
    { ok: false, status: 408 }
    );
}

/**
 * Sets network status (Network.online)
 * if the status is changed,
 * posts a message to all clients about current network status
 * @param  {bool}  online
 * @param  {int}  gap - milliseconds to lapse since last update
 * @set  Network = {online}
 * @calls  client.postMessage({online})
 */
const setNetworkOnline = (online = true, gap = 5000) => {
  if (typeof Network === 'undefined') {
    Console.log('No Network handler variable', 'SetNetworkOnline');
    return;
  }

  // current timestamp
  const now = Date.now();

  // if network state changes,
  // update the network state,
  // only update if Network.updated is either null (first run) 
  // or after a certain specified gap to avoid
  // constant network switching due to inconsistent network connectivity
  // then post a message to all clients
  (Network.online !== online) && 
  (
    Network.updated === null || 
    (Network.updated + gap) < now
  ) && 
  (Network = {online, updated: now}) && 
  self.clients.matchAll()
  .then(clients => {
    clients.forEach(client => {
      Console.log(Network, 'Network state change');
      client.postMessage(Network);
    })
  })
}

/**
 * Broadcast a message to all clients that
 * content update exists for a given URL
 * @param  {string}  url
 */
const notifyResponseUpdate = ({url}) => {
  self.clients.matchAll()
  .then(clients => {
    clients.forEach(client => {
      // Console.log(url, 'Updated Network Response');
      client.postMessage({url});
    })
  })
}

/**
 * Checks whether a particular fetch url is for a static asset
 * meaning an asset that is rarely updated
 * @param  {object}  
 * @param  {string}  object.url
 * @param  {array}  object.types
 */
const isStaticAsset = ({url = '', types = []}) => {
  const found = types.findIndex(type => url.includes(type));
  return (found >= 0);
}

/**
 * Dev Notes
 * 
 * REFRESH fetch method
 * Some response types, eg. images, could not be serialised when using
 * client.postMessage(), and will throw a DOMException: Response object 
 * could not be cloned. 
 * We are getting around this by notifying clients that an update exists
 * for a given URL, then the clients can submit a fetch event using the
 * REFRESH method, which then the SW will only serve up the updated
 * copy from the cache.
 * This of course requires that the cache be updated from the network 
 * before clients are notified.
 * 
 * todo: onsider using Transferrable interface of postMessage
 */